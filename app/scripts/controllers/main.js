'use strict';

/**
 * @ngdoc function
 * @name magicApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the magicApp
 */
angular.module('magicApp')
  .controller('MainCtrl', [
    '$scope',
    'Authentication',
    '$location',
    'userService',
    'dataService',
    'PubNub',
    '$rootScope',
    function($scope, Authentication, $location, userService, dataService, PubNub, $rootScope) {
      $scope.prevSlide = function(){
          $("#myCarousel").carousel("prev");
      };
      $scope.nextSlide = function(){
          $("#myCarousel").carousel("next");
      };

      $('a.page-scroll').bind('click', function(event) {
          var $anchor = $(this);
          $('html, body').stop().animate({
              scrollTop: ($($anchor.attr('href')).offset().top)
          }, 1250, 'easeInOutExpo');
          event.preventDefault();
      });

      $scope.sendMessage = function(){
        $scope.error = '';
        if (typeof $scope.username == 'undefined' || $scope.username == '') {
          $scope.error = 'Please input username';
        } else if (typeof $scope.email == 'undefined' || $scope.email == '') {
          $scope.error = 'Please input email';
        }else if (typeof $scope.message == 'undefined' || $scope.message == '') {
          $scope.error = 'Please input message';
        } else {
          // send email
        }
      }
    }
  ]);
