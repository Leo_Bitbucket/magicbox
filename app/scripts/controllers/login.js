'use strict';

/**
 * @ngdoc function
 * @name magicApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the magicApp
 */
angular
  .module('magicApp')
  .controller('LoginCtrl', [
    '$scope',
    '$location',
    'Authentication',
    function($scope, $location, Authentication) {
      //avoid multiple submit
      $scope.loginSending = false;
      //init the object first
      $scope.user = {
        username: '',
        password: ''
      };
      $scope.error = '';
      $scope.userLogin = function() {
        if ($scope.user.username !== '' && $scope.user.password !== '') {
          $scope.error = '';
          $scope.loginSending = true;
          Authentication.login($scope.user.username, $scope.user.password)
            .then(function(response) {
              $location.path('/');
            }).catch(function(error) {
              $scope.loginSending = false;
              $scope.error = 'Username or Password is incorrect!';
            });
        }
      };
    }
  ]);
