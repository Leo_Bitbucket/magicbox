'use strict';

angular.module('magicApp')
  .controller('ChatCtrl', [
    '$scope',
    'Authentication',
    '$location',
    'userService',
    'dataService',
    'PubNub',
    '$rootScope',
    function($scope, Authentication, $location, userService, dataService, PubNub, $rootScope) {

      $scope.logout = function() {
        Authentication.logout();
        $location.path('/login');
      };

      /* Pubnub */
      // make up a channel name
      $scope.channel  = 'public_channel';
      // pre-populate any existing messages
      $scope.chatMessages = [];

      if (!PubNub.initialized()) {
          // Initialize the PubNub service
          PubNub.init({
              publish_key: 'pub-c-7cfe275f-3dd5-4fb7-8094-c8deb4c089e2',
              subscribe_key: 'sub-c-8fd07a92-ef1b-11e5-8112-02ee2ddab7fe',
              uuid: 'Leo'
          });
      }

      // Subscribe to the Channel
      PubNub.ngSubscribe({ channel: $scope.channel });

      //publish text message to a particular channel
      $scope.textPublish = function() {
        var text = $scope.currentUser.sendMessage;
        if (text != undefined) {
          $scope.currentUser.sendMessage = '';
          var date = new Date();
          var json = {
            "uuid": "Leo",
            "senderId": $scope.currentUser.objectId,
            "type": "text",
            'messageText': text,
            'date': date.getHours() + ':' + date.getMinutes()
          };

          PubNub.ngPublish({
            channel: $scope.channel,
            message: json
          });
        }
      };

      // Register for message events
      $rootScope.$on(PubNub.ngMsgEv($scope.channel), function(ngEvent, payload) {
        $scope.$apply(function() {
          $scope.chatMessages.push(payload.message);
        });
      });

      // Register for presence events (optional)
      $rootScope.$on(PubNub.ngPrsEv($scope.channel), function(ngEvent, payload) {
        $scope.$apply(function() {
          $scope.chatUsers = PubNub.ngListPresence($scope.channel);
          //$scope.chatMessages.push(payload.message);
        });
      });

      // Pre-Populate the user list (optional)
      PubNub.ngHereNow({
        channel: $scope.channel
      });

      // Populate message history (optional)
      PubNub.ngHistory({
        channel: $scope.channel,
        count: 5
      });


    }
  ]);
