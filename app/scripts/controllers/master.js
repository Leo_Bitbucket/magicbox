'use strict';

angular.module('magicApp')
  .controller('MasterCtrl', [
    '$scope',
    'Authentication',
    '$location',
    'userService',
    'dataService',
    function($scope, Authentication, $location, userService, dataService) {

      $scope.isActive = function (viewLocation) {
          return viewLocation === $location.path();
      };
      console.log($location.path());

      $scope.logout = function() {
        Authentication.logout();
        $location.path('/login');
      };

    }
  ]);
