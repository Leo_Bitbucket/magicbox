'use strict';

angular.module('magicApp')
  .controller('UserCtrl', [
    '$scope',
    'Authentication',
    '$location',
    'userService',
    'dataService',
    '$rootScope',
    function($scope, Authentication, $location, userService, dataService, $rootScope) {

      $scope.tabs = ['Profile', 'Search User', 'Add User'];

      //tab control
      var currentTab = 'Profile';
      $scope.active = function(tab) {
        return tab === currentTab;
      };
      $scope.switchTab = function(tab) {
        currentTab = tab;
      };

      /* user profile */
      $scope.currentUser = Authentication.getCurrentUser();
      $scope.saveCustomerImage = function(customisedItemPicture) {
        $scope.imageUploading = true;
        $scope.currentUser.customisedItem.picture = customisedItemPicture;
        $scope.currentUser.currentProduct = $scope.currentUser.customisedItem;
        var reader = new FileReader();
        reader.onload = function(readerEvt) {
          var binaryString = readerEvt.target.result;
          $scope.currentUser.customisedItem.pictureData = btoa(
            binaryString);
        };
        reader.readAsBinaryString(customisedItemPicture);

        // update the user image using REST API in parse
        // imageUplad will upload the image to the parse server
        // imageRecord will update the record
        userService.imageUpload(customisedItemPicture).then(function(response){
          userService.imageRecord(response.name, $scope.currentUser.objectId, $scope.currentUser.sessionToken).then(
            function (result){
              $scope.currentUser.profileImage.url = result.data.profileImage.url;
              $scope.currentUser.profileImage.name = result.data.profileImage.name;
              Authentication.setCurrentUser($scope.currentUser);
              $scope.imageUploading = false;
              $scope.currentUser.customisedItem.picture = null;
            }
          );
        });
      }

      /* find user */
      $scope.userFinding = false;
      $scope.userSearchResults = [];
      $scope.warning = '';
      $scope.error = '';
      $scope.findUserByEmail = function() {
        //angularjs will not record the autocomplete value, so we can use jquery to get the value
        //or we can set autocomplete off
        $scope.email = $("#email").val();
        $scope.warning = '';
        $scope.error = '';
        if (typeof $scope.email == 'undefined' || $scope.email == '') {
          $scope.error = 'Please input an email!';
          return;
        }
        $scope.userFinding = true;
        var promise = userService.getUserByEmail($scope.email);
        promise.then(function(response) {
          if (response.results.length > 0) {
            $scope.warning = '';
            console.log(response.results[0]);
            $scope.userSearchResults.push(response.results[0]);
          } else {
            $scope.warning = 'User not existed!';
          }
          $scope.userFinding = false;
        });
      };

      /* advanced search */
      $scope.userTypeTags = [
        "support",
        "parent",
        "channel",
        "all"
      ];
      $scope.advancedSearch = function() {
        $scope.warning = '';
        $scope.error = '';
        $scope.userFinding = true;

        var queryJson = {};
        var location = $scope.query.location;
        if(location!=''&&location!=undefined){
          queryJson.suburbGeoPoint = {
              "__type":"GeoPoint",
              "latitude":location[0],
              "longitude":location[1]
          }
        }
        var userType = $scope.query.selectedTag;
        if(userType!=undefined&&userType!=="all"){
          queryJson.userType = userType;
        }
        var dateStart = $scope.query.dateStart;
        var dateEnd = $scope.query.dateEnd;
        if(dateStart !== undefined&&dateStart !== ''){
          queryJson.startDate = new Date(dateStart);
        }

        if(dateEnd!=undefined&&dateEnd!=''){
          queryJson.endDate = new Date(dateEnd);
        }

        var promise = userService.advancedSearch(queryJson);
        promise.then(function(response) {
          console.log(response);
          if (response.data.result.length > 0) {
            $scope.warning = '';
            $scope.userSearchResults=response.data.result;
          } else {
            $scope.warning = 'User not existed!';
          }
          $scope.userFinding = false;
        });
      };

      /* add new user */
      $scope.addUserError = '';
      $scope.successMessage = '';
      $scope.addNewUser = function(){
        $scope.addUserError = '';
        $scope.successMessage = '';
        if (typeof $scope.newUser == 'undefined') {
          $scope.addUserError = 'Please input the username';
          return;
        }
        if (typeof $scope.newUser.username == 'undefined' || $scope.newUser.username == '') {
          $scope.addUserError = 'Please input the username';
          return;
        }
        if (typeof $scope.newUser.password == 'undefined' || $scope.newUser.password == '') {
          $scope.addUserError = 'Please input the password';
          return;
        } else if($scope.newUser.password != $scope.newUser.repeatPassword) {
          $scope.addUserError = 'Password not equal';
          return;
        }
        if (typeof $scope.newUser.email == 'undefined' || $scope.newUser.email == '') {
          $scope.addUserError = 'Please input the email';
          return;
        } else if($scope.newUser.email != $scope.newUser.repeatEmail) {
          $scope.addUserError = 'Email not equal';
          return;
        }
        userService.addNewUser($scope.newUser).then(
          function(response){
          $scope.successMessage = 'New user has been created successfully!';
        }, function(error){
          $scope.addUserError = error.data.error;
        });
      }


      $scope.logout = function() {
        Authentication.logout();
        $location.path('/login');
      };

    }
  ]);
