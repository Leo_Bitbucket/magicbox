'use strict';

angular.module('magicApp')
.directive('googlePlace',function(){
	return{
		link: function(scope, element) {
			var place = new google.maps.places.Autocomplete(element[0]);
			google.maps.event.addListener(place, 'place_changed', function() {
				if(place.getPlace().name !== undefined &&
          scope.query.userLocation !== "" &&
          scope.query.userLocation !== undefined){
					var geometry = place.getPlace().geometry;
					if(geometry === undefined){
						window.alert('This location does not have Geometry!');
						return false;
					}
					scope.query.location = [geometry.location.lat(),geometry.location.lng()];
				}
			});
		}
	};
});
