'use strict';

angular.module('magicApp')
	.factory('userService', [
		'$http',
		'dataService',
		'$q',
		'Upload',
		'ENV',
		function($http, dataService, $q, Upload, ENV) {
			var config;
      dataService.getConfig().then(function(server_res){
        config = server_res.data;
      });

			return {

				getUserByEmail: function(email) {
					var PARSE_HEADER = {
						'Content-Type': 'application/json'
					};

					PARSE_HEADER = angular.extend(PARSE_HEADER, config[ENV.name].header);

					var query = 'where=' + JSON.stringify({
						'email': email
					});

					var req = {
						method: 'GET',
						headers: PARSE_HEADER,
						url: config.apiEndpoint + '1/users?' + encodeURI(query)
					};

					var promise = $http(req).then(
						function(response) {
							return response.data;
						});
					return promise;
				},

				imageUpload: function(imageFile){
					var fileName = imageFile.name;
					var PARSE_HEADER = {
						'Content-Type': 'image/jpeg'
					};
					PARSE_HEADER = angular.extend(PARSE_HEADER, config[ENV.name].header);

					return $q(function(resolve,reject){
						Upload.http({
							url:'https://api.parse.com/1/files/'+fileName,
							method:"POST",
							data:imageFile,
							headers:PARSE_HEADER
						}).success(function(response){
							resolve(response);
						}).error(function (response) {
							console.log(response);
						})
					})
				},

				imageRecord:function(name, userId, sessionToken){
					var PARSE_HEADER = {
						'Content-Type': 'application/json',
						'X-Parse-Session-Token': sessionToken
					};

					PARSE_HEADER = angular.extend(PARSE_HEADER, config[ENV.name].header);
					var req = {
						method: 'PUT',
						headers: PARSE_HEADER,
						url: config.apiEndpoint + '1/users/' + userId,
						data:{
							'profileImage':{
								'name':name,
								'__type':'File'
							}
						}
					};

					var promise = $http(req).then(
						function(response){
							return response;
						},
					  function(error){
							console.log(error);
						}
					);
					return promise;
				},

				advancedSearch: function(queryJson) {
					var PARSE_HEADER = {
						'Content-Type': 'application/json'
					};

					PARSE_HEADER = angular.extend(PARSE_HEADER, config[ENV.name].header);
					console.log(queryJson);
					var req = {
						method: 'POST',
						headers: PARSE_HEADER,
						url: config.apiEndpoint + '1/functions/advancedSearch',
						data: queryJson
					};
					var promise = $http(req).success(function(response) {
						return response;
					}).error(function(error){
						console.log(error);
					});
					return promise;
				},

				addNewUser: function(newUser){
					var PARSE_HEADER = {
						'Content-Type': 'application/json',
						'X-Parse-Revocable-Session': 1
					};

					PARSE_HEADER = angular.extend(PARSE_HEADER, config[ENV.name].header);
					var req = {
						method: 'POST',
						headers: PARSE_HEADER,
						url: config.apiEndpoint + '1/users',
						data:{
							"username":newUser.username,
							"password":newUser.password,
							"email":newUser.email
						}
					}
					return $q(function(resolve,reject){
						$http(req).then(
							function(response){
								resolve(response);
							},
						  function(error){
								reject(error);
							}
						);
					});
				}

			};
		}
	]);
