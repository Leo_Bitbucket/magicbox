'use strict';

angular.module('magicApp')
  .factory('Authentication', [
    '$cookies',
    '$q',
    '$http',
    'dataService',
    'ENV',
    function($cookies, $q, $http, dataService, ENV){
      var config;
      dataService.getConfig().then(function(server_res){
        config = server_res.data;
      });

      var currentUser = {};
      if ($cookies.get('magic_user')) {
        currentUser = JSON.parse($cookies.get('magic_user'));
      }

      return {
        authenticate: function(username, password){
          var PARSE_HEADER = {
            'Content-Type': 'application/json'
          };

          PARSE_HEADER = angular.extend(PARSE_HEADER, config[ENV.name].header);

          var query = 'username=' + username + '&password=' + password;

          var req = {
            method: 'GET',
            headers: PARSE_HEADER,
            url: config.login_url + encodeURI(query)
          };
    			var promise = $http(req).success(function(data) {
            return data;
          });

          return promise;

        },

        login: function(username, password){
          var deferred = $q.defer();
          this.authenticate(username, password).then(function(response){
              currentUser = response.data;
              $cookies.put('magic_user', JSON.stringify(currentUser));
              deferred.resolve(response.data);
          },function(error){
            deferred.reject(error);
          });
          return deferred.promise;
        },

        logout: function(){
          $cookies.remove('magic_user');
          currentUser = {};
        },

        isLoggedIn: function(callback){
          if (currentUser.hasOwnProperty('username')) {
            callback(true);
          }else {
            callback(false);
          }
        },

        getCurrentUser: function(){
          return currentUser;
        },

        setCurrentUser: function(newUser){
          currentUser = newUser;
          if ($cookies.get('magic_user')) {
            var oldUser = JSON.parse($cookies.get('magic_user'));
            oldUser.profileImage.url = newUser.profileImage.url;
            oldUser.profileImage.name = newUser.profileImage.name;
            document.cookie = "magic_user=" + JSON.stringify(oldUser);
          }
        }
      };
    }]);
