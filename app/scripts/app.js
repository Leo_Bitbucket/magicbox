'use strict';

/**
 * @ngdoc overview
 * @name magicApp
 * @description
 * # magicApp
 *
 * Main module of the application.
 */

angular
  .module('magicApp', [
    'environment',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.sortable',
    'ngMaterial',
    'pubnub.angular.service',
    'ngFileUpload'
  ])
  .config(function($routeProvider) {
    $routeProvider
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/user', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl',
        controllerAs: 'user'
      })
      .when('/chat', {
        templateUrl: 'views/chat.html',
        controller: 'ChatCtrl',
        controllerAs: 'chat'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).run(function($rootScope, $location, Authentication) {
    //Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$routeChangeStart', function() {
      Authentication.isLoggedIn(function(response) {
        if (!response) {
          event.preventDefault();
          $location.path('/login');
        }
      });
    });
  });
